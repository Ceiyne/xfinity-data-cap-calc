console.log('XDCC: Xfinity Data Cap Calc extension loaded');

function doWork() {
  let div = document.querySelector('div.usage-info__monthlyInfo');

  if (!div) {
    console.log("XDCC: null div");
    return;
  }

  if (div.childNodes.length < 2) {
    console.log("XDCC: insufficient div child nodes");
    return;
  }

  let text = div.childNodes[1].innerText;

  let alreadyDoneCheck = text.match(/You have used/);
  if (!!alreadyDoneCheck) {
    console.log("XDCC: already inserted text, exiting");
    return;
  }

  console.log("XDCC: text not found, proceeding");

  let values = text.match(/\d+/g);

  if (!values) {
    console.log("XDCC: null values");
    return;
  }

  if (values.length < 4) {
    console.log("XDCC: insufficient values");
    return;
  }

  let remainingData = Number(values[0]);
  let totalData = Number(values[1]);
  let lastDay = Number(values[3]);

  let perDay = totalData / lastDay;
  let currDay = new Date().getDate();

  // if no plan data is found (unlimited plan) this will be a day of the month, so check for <= 31 (no one has a 31mb cap)
  if (totalData <= 31) { 
    console.log("XDCC: plan data not found");
    return;
  }

  // ###########################################################################################
  // Uncomment one of the following lines to test warning highlights for projected data overage
  // remainingData = totalData - Math.round(perDay * currDay * 1.1);
  // remainingData = totalData - Math.round(perDay * currDay * 0.95);
  // remainingData = totalData - Math.round(perDay * currDay * 0.75);

  let usedData = totalData - remainingData;
  let projData = Math.round(lastDay * usedData / currDay);
  let daysWorthUsed = Math.round(lastDay * usedData / totalData * 10) / 10;
  let percentUsed = Math.round(usedData / totalData * 100) / 100;
  let formattedPercentUsed = percentUsed.toLocaleString('en-US', { style: 'percent' });
  let projPercentUsed = Math.round(projData / totalData * 100) / 100;
  let formattedProjPercentUsed = projPercentUsed.toLocaleString('en-US', { style: 'percent' });
  let remainingDays = lastDay - currDay;
  let adjustedPerDay = remainingDays === 0 ? remainingData : Math.round(remainingData / remainingDays * 10) / 10;

  let warningColor =
    projPercentUsed > 1 ? "red" :
    projPercentUsed > 0.9 ? "orange" :
    "green";

  let html = div.childNodes[1].innerHTML;
  div.childNodes[1].innerHTML = html.replace(').',
    `
      ).<br>
      You have used <b style="color: ${warningColor}">${usedData}GB (${formattedPercentUsed})</b> of data 
      <b style="color: ${warningColor}">(${daysWorthUsed} days worth).</b> 
      Your projected data usage is <b style="color: ${warningColor}">${projData}GB (${formattedProjPercentUsed})</b>. 
      You can use up to <b style="color: ${warningColor}">${adjustedPerDay}GB per day</b> and still remain below the data cap.
    `);
}

let observer = new MutationObserver((mutationList, observer) => {
  if (document.querySelector('div.usage-info__monthlyInfo') !== null) {
    console.log('XDCC: inserting text');
    //observer.disconnect(); // stop observing
    doWork();
  }
})

observed = document.querySelector('body');
if (!observed) {
    console.log("XDCC: null observed");
  }
  else {
    observer.observe(observed, {subtree: true, childList: true});
  }
